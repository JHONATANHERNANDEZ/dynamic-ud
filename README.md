# Analysis of dynamic systems

## Course on Analysis of dynamic systems prepared by the ARMOS research group.

The course of **Analysis of Dynamic Systems** imparted in the Universidad Distrital, Tecnología en Electricidad, with code supported in Jupyter Notebook.

17-02-19 Inclusion of Laplace transform.

07-03-20 The files in the repository were updated.
